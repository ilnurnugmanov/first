<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$productIDs=[];
foreach ($arResult['ITEMS'] as $ID => $arItem)
{
    $itemImage = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>$arParams['PREV_PICTURE_WIDTH'], 'height'=>$arParams['PREV_PICTURE_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult['ITEMS'][$ID]['PREVIEW_PICTURE'] = $itemImage;
    $productIDs[] = $arItem['PROPERTIES']['LINK']['VALUE'];
}

$arSelect = Array("ID", "NAME", "PROPERTY_PRICE", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>IntVal(2), "ACTIVE"=>"Y", "ID" => $productIDs);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
    $arFields = $ob->GetFields();
    $arResult['PRODS'][$arFields["ID"]] = $arFields;
}
