<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script>
    $().ready(function(){
        $(function(){
            $('#slides').slides({
                preload: false,
                generateNextPrev: false,
                autoHeight: true,
                play: 4000,
                effect: 'fade'
            });
        });
    });
</script>
<div class="sl_slider" id="slides">
    <div class="slides_container">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?$prodID = $arItem["PROPERTIES"]['LINK']['VALUE'];?>
            <div>
                <div>
                    <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" alt="" />
                    <?endif;?>
                    <h2><a href="<?=$arResult['PRODS'][$prodID]['DETAIL_PAGE_URL']?>"><?echo $arItem["NAME"]?></a></h2>
                    <p><?echo $arResult['PRODS'][$prodID]["NAME"]." всего за " . $arResult['PRODS'][$prodID]["PROPERTY_PRICE_VALUE"] . " руб";?></p>
                    <a title="<?=$arResult['PRODS'][$prodID]["NAME"]?>" href="<?=$arResult['PRODS'][$prodID]['DETAIL_PAGE_URL']?>" class="sl_more">Подробнее &rarr;</a>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>

