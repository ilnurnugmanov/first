<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html lang="<?=LANGUAGE_ID?>>
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>
        <?
        $APPLICATION->AddHeadScript('/local/templates/.default/js/jquery-1.8.2.min.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slides.min.jquery.js');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.carouFredSel-6.1.0-packed.js');
        $APPLICATION->AddHeadScript('/local/templates/.default/js/functions.js');

        $APPLICATION->SetAdditionalCSS("/local/templates/.default/template_styles.css");
        ?>
    <link rel="shortcut icon" type="image/x-icon" href="/local/templates/.default/favicon.ico">

    <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<div class="wrap">
    <div class="hd_header_area">
        <?include_once $_SERVER['DOCUMENT_ROOT'].'/local/templates/.default/include/header.php'?>
    </div>
